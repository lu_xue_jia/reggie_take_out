# 瑞吉外卖

#### 介绍
![输入图片说明](img/image2.png)

#### 软件架构
![输入图片说明](img/image.png)


#### 安装教程

1.  下载项目。项目使用了lombok插件，ide怎么安装lombok，请自行安装
2.  修改application.yml中对应的mysql配置和redis配置信息
3.  运行com.itheima.reggie.ReggieApplication，启动项目
4.  访问地址http://localhost:8080/


#### 使用技术
1. 服务端
    >> springboot
    
    >> mysql
    
    >> redis
    
    >> nginx
    
    >> mybatis-plus
    
2. 前端
    >> vue
    
    >> H5

#### 参考

1.  https://www.bilibili.com/video/BV13a411q753